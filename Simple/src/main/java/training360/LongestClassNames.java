package training360;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

public class LongestClassNames {

	public static final String CLASS_NAME_REGEX = "[./]([^/^.]*).html\"";

	public List<String> getLongestClassNames(String url, int limit) {
		String content = null;
		try {
			content = Resources.toString(new URL(url), Charsets.UTF_8);
			Pattern p = Pattern.compile(CLASS_NAME_REGEX);
			Matcher m = p.matcher(content);
			List<String> longestClassNames = new ArrayList<>();
			while (m.find()) {
				String className = m.group(1);
				if (className.length() > limit) {
					longestClassNames.add(className);
				}
			}
			System.out.println(longestClassNames.size());
			
			return longestClassNames.stream()
					.sorted(Comparator.comparing(String::length).reversed())
					.limit(limit)
					.collect(Collectors.toList());

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Cant download this", e);
		}
		return null;

	}
}
