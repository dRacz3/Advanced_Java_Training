package simple;

import java.util.List;

import org.junit.Test;

import training360.LongestClassNames;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.*;

public class LongestClassNamesTest {
	@Test
	public void testGetLongestClassNames() {
		LongestClassNames helloWorld = new LongestClassNames();
		String url = "http://docs.oracle.com/javase/8/docs/api/allclasses-frame.html";
		List<String> classNames = helloWorld.getLongestClassNames(url, 5);
		assertThat(classNames.size(), equalTo(5));
		assertThat(classNames.get(0), equalTo("SQLIntegrityConstraintViolationException"));
	}
}
