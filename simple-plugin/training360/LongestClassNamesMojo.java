package jtechlog.classnames;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Mojo(name = "classnames")
public class ClassNamesMojo extends AbstractMojo {

    @Parameter( property = "classnames.url", defaultValue = "https://docs.oracle.com/javase/8/docs/api/allclasses-frame.html" )
    private String url;

    @Parameter( property = "classnames.limit", defaultValue = "0" )
    private int limit;

    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info(longestClassNames(url, limit).toString());
    }

    public static final String CLASS_NAME_REGEX = "[./]([^/^.]*).html\"";

    public List<String> longestClassNames(String url, int limit) {
        try {
            String content = Resources.toString(new URL(url), Charsets.UTF_8);
            Pattern p = Pattern.compile(CLASS_NAME_REGEX);
            Matcher m = p.matcher(content);
            List<String> longestClassNames = new ArrayList<>();
            while (m.find()) {
                String className = m.group(1);
                if (className.length() > limit) {
                    longestClassNames.add(className);
                }
            }
            return longestClassNames;
        }
        catch (IOException ioe) {
            throw new RuntimeException("Error getting " + url, ioe);
        }

    }

}