
__Annotation__: extra adat tárolása  
pl: Functional interface, override, @test stb.  
```java
@Documented // If there is any, attach javadoc
@Target(ElementType.Field)
@Inherited // Parent attribute goes to child as well
@Retention(RetentionPolicy.RUNTIME) // if runtime-> reflection can access it
public @interface ValidatedField{
  int minValue() default Integer.MIN_VALUE;
  String value() default "";
  Class[] validators() default{};
  ``` 
