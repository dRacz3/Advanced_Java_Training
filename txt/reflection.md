# Reflection
Használata: osztály betöltése futásidőben előálló név string alapján.
Nem a barátunk, keretrendszer fejlesztésre jó amúgy -> Üzleti alkalmazás fejelsztésben nem használunk.


Mire jó: 
Osztály betölthető név alapján.
Létrehozni az osztályt.
Osztály felépítését elemezni tudjuk és módosítani.

Minden referencia tipushoz létrejön egy class példány (Class, interface, enum, array) + -> primitv tipusokhoz + voidhoz<- (ezekből egy) 
PermGenbe (új neve : MetaSpace) tölti be ezeket a jvm

```java
Class clazz = new Trainer(0l, "", 12).getClass();
``` 
vagy

```java
Class clazz = Class.forName("training360.Trainer");
``` 

 vagy primitiveknél: :
```java
Class clazz = int.class;
``` 
Példányosítás:
```java
clazz.newInstance();
``` 

Tartalmazott mindent: (1) -> publikus, (2)-> privátot is(örököltet nem)
```java
Class.getClasses()
Class.getDeclaredClasses()
``` 

Őslekérdezés, Interfacek lekérése. (Tranzitív dolgokat nem, ahhoz rekurzív keresés kell):
```java
Class.getSuperClass()
Class.getInterfaces()
``` 


Metódus hívás pl egy Appender osztály esetén:
(Betöltés Reflectionnel, de utána tipusbiztos használat)
```java
Appender a = (Appender) clazz.newInstance();
a.kutymurutty();
``` 

Privtát attributumot:
```java
field.setAccessible(true)
``` 

