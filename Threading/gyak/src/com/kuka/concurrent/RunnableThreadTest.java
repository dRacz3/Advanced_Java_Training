package com.kuka.concurrent;

public class RunnableThreadTest {

	public static void main(String[] args) {
		Runnable r1 = new PrintNameRunnable("A");
		Runnable r2 = new PrintNameRunnable("B");
		Runnable r3 = new PrintNameRunnable("C");
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		Thread t3 = new Thread(r3);
		t1.start();
		t2.start();
		t3.start();
	}

}
