package com.kuka.concurrent;

import java.util.Random;

/*
 * Thread extending the Thread base class
 */
public class PrintNameRunnable extends Thread {

	public PrintNameRunnable(String name) {
		super(name);
	}

	@Override
	public void run() {
		Random r = new Random();
		int randomMillis = r.nextInt(3000);
		try {
			Thread.sleep(randomMillis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < 10; ++i) {
			System.out.println(String.format("%s%d", getName(), i));
		}
	}
}
