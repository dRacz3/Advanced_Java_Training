package com.kuka.concurrent;

public class RunnableThreadTest1b2 {

	public static void main(String[] args) {

		Thread t1 = new PrintNameRunnable("A");
		Thread t2 = new PrintNameRunnable("B");
		Thread t3 = new PrintNameRunnable("C");
		System.out.println("Running threads...");
		t1.start();
		t2.start();
		t3.start();

		System.out.println("All treads finished..");
	}

}
