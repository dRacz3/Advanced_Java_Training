package com.kuka.concurrent;

public class RunnableThreadTest1b {

	public static void main(String[] args) {

		Runnable r1 = new PrintNameRunnable("A");
		Runnable r2 = new PrintNameRunnable("B");
		Runnable r3 = new PrintNameRunnable("C");
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		Thread t3 = new Thread(r3);
		System.out.println("Running threads...");
		t1.start();
		t2.start();
		t3.start();

		try {
			t1.join();
			t2.join();
			t3.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println("All treads finished..");
	}

}
