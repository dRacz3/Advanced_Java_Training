package com.kuka.concurrent;

import java.util.Random;

public class PrintNameRunnable implements Runnable {

	String name;

	public PrintNameRunnable(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		Random r = new Random();
		int randomMillis = r.nextInt(3000);
		try {
			Thread.sleep(randomMillis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < 10; ++i) {
			System.out.println(String.format("%s%d", name, i));
		}
	}
}
