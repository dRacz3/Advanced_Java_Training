package com.kuka.concurrence;

public class SimpleExec implements Runnable {

	private String name;

	public String getName() {
		return name;
	}

	public SimpleExec(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		System.out.printf("%s, %d \n"
				, name
				, Thread.currentThread().getPriority());
		System.out.printf("%s has finished execution. \n", name);
	}

}
