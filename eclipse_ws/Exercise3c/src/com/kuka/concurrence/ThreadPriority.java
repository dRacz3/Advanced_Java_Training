package com.kuka.concurrence;


public class ThreadPriority {

	public static void main(String[] args) {

		Thread t1 = new Thread(new SimpleExec("Boston"));
		Thread t2 = new Thread(new SimpleExec("New York"));
		Thread t3 = new Thread(new SimpleExec("Tokyo"));

		t1.setName("Boston"); t1.setPriority(Thread.MAX_PRIORITY);
		t2.setName("New York");t2.setPriority(Thread.NORM_PRIORITY);
		t3.setName("Tokyo");t3.setPriority(Thread.MIN_PRIORITY);

		t1.start();
		t2.start();
		t3.start();

		// enumerate false -> non recursive, true-> recursive


		try {
			t1.join();
			t2.join();
			t3.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println("Finished!");
	}

}
