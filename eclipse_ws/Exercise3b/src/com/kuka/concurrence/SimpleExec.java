package com.kuka.concurrence;

public class SimpleExec implements Runnable {

	private String name;
	
	
	
	public String getName() {
		return name;
	}



	public SimpleExec(String name) {
		this.name = name;
	}



	@Override
	public void run() {
		for(int i=0; i<5; ++i)
		{
			System.out.println(name);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		System.out.printf("%s has finished execution. \n", name);
	}

}
