package com.kuka.concurrence;

public class ThreadGroupTest2 {

	public static void main(String[] args) {
		Thread[] allThreads = findAllThreads(null);
		for (int i = 0; i < allThreads.length; ++i) {
			if (allThreads[i] == null) {
				break;
			}
			System.out.println(allThreads[i].getName());
		}

	}

	public static Thread[] findAllThreads(ThreadGroup currrentTg) {
		ThreadGroup threadGrp = currrentTg == null
				? Thread.currentThread().getThreadGroup()
				: currrentTg;
		ThreadGroup parent = threadGrp.getParent();
		Thread[] allThreads = new Thread[10];
		int allThreadCnt = 0;

		while (parent != null && currrentTg != null) {
			threadGrp = parent;
			parent = threadGrp.getParent();
		}

		allThreadCnt = threadGrp.enumerate(allThreads);
		while (allThreadCnt == allThreads.length) {
			allThreads = new Thread[allThreadCnt * 2];
			allThreadCnt = threadGrp.enumerate(allThreads);
		}
		return allThreads;
	}

	public static void printAllThreadsWithinThreadGroup() {
		ThreadGroup tg = Thread.currentThread().getThreadGroup();
		ThreadGroup parent = tg.getParent();
		ThreadGroup[] allThreadGroups = new ThreadGroup[10];
		int allThreadGroupCnt = 0;

		while (parent != null) {
			tg = parent;
			parent = tg.getParent();
		}

		allThreadGroupCnt = tg.enumerate(allThreadGroups, true);
		while (allThreadGroupCnt == allThreadGroups.length) {
			allThreadGroups = new ThreadGroup[allThreadGroupCnt * 2];
			allThreadGroupCnt = tg.enumerate(allThreadGroups, true);
		}

		for (int i = 0; i < allThreadGroupCnt; ++i) {
			System.out
					.println("Thread gRoup : " + allThreadGroups[i].getName());
			Thread[] t = findAllThreads(allThreadGroups[i]);
			for (int j = 0; j < t.length; ++j) {
				if (t[j] == null) {
					break;
				}
			}
		}
	}
}
