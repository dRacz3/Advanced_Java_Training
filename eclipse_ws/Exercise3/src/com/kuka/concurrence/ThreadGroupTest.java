package com.kuka.concurrence;

public class ThreadGroupTest {

	public static void main(String[] args) {
		// ThreadGroup belongs to the place where it is created
		ThreadGroup ownGroup = new ThreadGroup(Thread.currentThread().getThreadGroup(), "CitiesGroup");
		ThreadGroup mainGroup = Thread.currentThread().getThreadGroup();

		int groupCnt = 0;

		Thread t1 = new Thread(ownGroup,
				new SimpleExec("Boston"));
		Thread t2 = new Thread(ownGroup,
				new SimpleExec("New York"));
		Thread t3 = new Thread(ownGroup,
				new SimpleExec("Tokyo"));

		Thread[] threads = new Thread[10];

		t1.setName("Boston");
		t2.setName("New York");
		t3.setName("Tokyo");
		
		t1.start();
		t2.start();
		t3.start();
		
		// enumerate false -> non recursive, true-> recursive
		groupCnt = mainGroup.enumerate(threads, true);
		
		for(int i = 0; i< groupCnt; ++i)
		{
			System.out.println("Thread: " + threads[i].getName());
		}
		
		System.out.println("Current thread count in mainGroup : "
				+ mainGroup.activeCount());
		
		
		try {
			t1.join();
			t2.join();
			t3.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		System.out.println("Finished!");
	}

}
