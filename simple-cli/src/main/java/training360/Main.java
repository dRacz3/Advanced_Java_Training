package training360;

import java.util.List;


public class Main {
	public static void main(String[] args)
	{
		String url = "http://docs.oracle.com/javase/8/docs/api/allclasses-frame.html";
		int limit = 10;
		
		LongestClassNames classNames = new LongestClassNames();
		List<String> names = classNames.getLongestClassNames(url, limit);
	}
}
